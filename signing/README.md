# Configuration of the KDE CI Notary Services

The `*.ini` files specify the general settings for the services and the corresponding
clients used by the KDE CI/CD pipelines. The `*-projects.yaml` files specify
the project-specific settings for the services.

For all projects that want to use some of the services, one or more branches
must be cleared for this service. This is done by adding the project and the
desired branches to the service's `*-projects.yaml` file.

## Project-specific Settings

The project-specific settings consist of two parts:
* Default settings shared by all projects (e.g. the signing key) and branches
for which the service can be used by all projects;
* Settings that are specific for a project (or a project's branch) and that
override the defaults. And branches for which the service can be used additionally
to the default branches.

All settings can be specified globally for all projects (below `defaults`),
for all branches of one project (below the project's identifier), and
for one branch of one project (below the branch name in the project settings).

The top-level project identifier must match the path of the project
([CI_PROJECT_PATH](https://invent.kde.org/help/ci/variables/predefined_variables.md))
on invent.kde.org, e.g. `pim/itinerary`.

## Project-specific Settings for Android Services

For all projects, the application ID (e.g. `org.kde.itinerary`) must be specified
as `applicationid` below the project's entry. The services verify that the
application ID matches the ID of the APK.

### apksigner

The project-specific settings are specified in `apksigner-projects.yaml`. For
all projects, APKs built from the master branch are signed. If you also want to sign
the APKs built from the latest release branch, then add the name of this branch
to the `branches` dictionary of the project's entry, e.g.

```yaml
pim/itinerary:
  applicationid: org.kde.itinerary
  branches:
    release/23.08:
```

Note that the branch below `branches` is specified as (empty) dictionary.

Additionally, you will most likely have to specify the file name of the keystore which
contains the key to sign your app and the file name of a file containing the password
protecting the keystore. Google recommends to use separate signing keys for different
apps. Submit a [sysadmin ticket](https://community.kde.org/Sysadmin) to get the keystore file on the system providing the
signing service or to ask for a new key(store) to be created for you app.

### fdroidpublisher

The project-specific settings are specified in `fdroidpublisher-projects.yaml`. For
all projects, APKs built from the master branch are published in [KDE's F-Droid Nightly
Build repository](https://community.kde.org/Android/F-Droid#KDE_F-Droid_Nightly_Build_Repository).
If you also want to publish the APKs built from the latest release branch,
then add the name of this branch to the `branches` dictionary of the project's entry.
Additionally, you have to specify the `stable-releases` repository for this branch,
so that the stable builds of your app are published in [KDE's F-Droid Release
repository](https://community.kde.org/Android/F-Droid#KDE_F-Droid_Release_Repository).

Example:
```yaml
pim/itinerary:
  applicationid: org.kde.itinerary
  branches:
    release/23.08:
      repository: stable-releases
```
